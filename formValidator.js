/** @fileoverview FormValidator jQuery Plugin 
 *  @author  Arun Shourie Balakrishnan
 *  @version 1.0
 *  @requires jQuery, jQuery UI Widget factory

 * Enables to add custom validations without much effort.
 */

(function ($) {

    $.widget('ar.formvalidator', {

        /*****************
        * Default values
        ******************/

        options: {

            /*************************
    
            * Default Messages

            *************************/

            messages: {
                

                'required': "This field is required.",

                'email': 'Please use the standard format name@domain.com.',

                'name': 'Names may only contain letters A-Z.',

                'city': 'City may only contain upper or lower case letters, commas, spaces, hyphens, and periods.',

                'addr': 'Address may only contain alphanumeric characters.',

                'phone': 'Please enter any 10 digit US domestic number. Valid formats: 5555555555 or 555-555-5555 or (555) 555-5555.',

                'extn': 'Please enter a numeric extension.',

                'zip': 'Please enter zip code.',

                'date' : 'Please enter a valid date'

            },

            /*************************************
       
            * Validation Rules and Reset Rules
       
            **************************************/

            validationrules: {

                required: {
                    rule: function () {
                        var $this = $(this);
                        if ( $this.is(":radio") || $this.is(":checkbox") ) {
                            var thisName = $this.attr('name');
                            if ( thisName ) {
                                return ( $("input[name="+ thisName +"]:checked").length ) ? true : false;
                            }
                            return false;
                        }
                        //Check for input type text or Select Box.
                        else {
                            return ( $.trim( $this.val()) !== '' ) ? true : false;
                        }
                    },
                    resetrule: function () {

                    }
                },

                email: {
                    rule: function () {
                        return /^(?=.{0,64}@)(([\w-\"]+(?:\.[\w-\"]+)*))@((?:[\w-]+\.)*\w[\w-]{0,256})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test($(this).val()) ? true : false;
                    }
                },

                name: {
                    rule: function () {
                        return /^[A-Za-z\.,\s -]{2,25}$/.test($(this).val()) ? true : false;
                    }
                },

                city: {
                    rule: function () {
                        return /^[A-Za-z\.,\s -]{1,25}$/.test($(this).val()) ? true : false;
                    }
                },

                addr: {
                    rule: function () {
                        return /^([a-zA-Z0-9 ,-\.]+)$/.test($(this).val()) ? true : false;
                        //return true;
                    }
                },

                phone: {
                    rule: function () {
                        return /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test($(this).val()) ? true : false;
                    }
                },

                date : {

                    rule : function() {

                        var s = $(this).val();
                        // make sure it is in the expected format
                        if (s.search(/^\d{1,2}[\/|\-|\.|_]\d{1,2}[\/|\-|\.|_]\d{4}/g) != 0)
                         return false;

                        // remove other separators that are not valid with the Date class   
                        s = s.replace(/[\-|\.|_]/g, "/");

                        // convert it into a date instance
                        var dt = new Date(Date.parse(s));    

                        // check the components of the date
                        // since Date instance automatically rolls over each component
                        var arrDateParts = s.split("/");
                         return (
                             dt.getMonth() == arrDateParts[0]-1 &&
                             dt.getDate() == arrDateParts[1] &&
                             dt.getFullYear() == arrDateParts[2]
                        );
                         
                    }
                },
                extn: {
                    rule: function () {
                        return /^\d{4}$/.test($(this).val()) ? true : false;
                    }
                },

                zip: {
                    rule: function () {
                        return /^\d{5}$|^\d{5}-\d{4}$/.test($(this).val()) ? true : false;
                    }
                }

            },


            /****************************

            * Default Submit Handler 

            ****************************/

            submitHandler: function () {

                return true;

            },

            errorLabels : {
                'highLight':'.lblFieldPair',
                'container' : '.input'
            },

            /*******************************************************************************************************************

            * Global Error Handler . Will get two parameters , the current form element ( $inputElement ) and the error message.

            **********************************************************************************************************************/
            errorMessageHandler: function ($errorContainer, errormessage) {
                var alertErrorClass = this.options.errorElement;
                $errorContainer.find('.alertText').remove();
                if ( $errorContainer.is('input') || $errorContainer.is('select')) {
                    $('<span class="alertText">' + errormessage + '</span>').insertAfter($errorContainer);
                }
                else {
                    $errorContainer.append('<span class="alertText">' + errormessage + '</span>');
                }
            }, 

            validationSuccessHandler: function () { },

            customErrorHandler: function () { }

            /*********** End of options object ***************/
        },

        /**********************************************

        * Initialize the Form validator widget ....

        *********************************************/
        _create: function () {

            var element = this.element.get(0);

            if (element.nodeName === "FORM" || this.element.hasClass('validate-form')) {

                /****************************************
                * Add the Form validator to the form
                ****************************************/
                this._attachValidator();
                this._validationErrorHandler = this.options.errorMessageHandler;
                this._validationSuccessHandler = this.options.validationSuccessHandler;

            }

        },

        _findErrorContainer: function ($input) {

            /****************************************************************
            * Find the element to which the error message should be appended
            *****************************************************************/
            var errorLabelContainer = this.options.errorLabels.container;
            if ( errorLabelContainer !== "") {
                return $input.closest(errorLabelContainer);
            }
            else {
                return $input;
            }
            

        },

        _onErrorDoThis: function ($input, rule) {

            var errormsg = "",
                self = this,
                customerrorDataIdentifier = $input.data('vid'),
                $errorContainer = this._findErrorContainer($input),
                errorHighLightClass = this.options.errorLabels.highLight;

            if (customerrorDataIdentifier !== undefined) {

                errormsg = this.options.messages[customerrorDataIdentifier][rule];

                if (errormsg === undefined) {
                    errormsg = this.options.messages[rule];
                }

            }
            else {
                errormsg = this.options.messages[rule];
            }

            if (errormsg !== undefined) {

                if ( $errorContainer.length > 0 ) {
                    self._validationErrorHandler($errorContainer, errormsg);
                }
                          
            }
            $input.closest(errorHighLightClass).addClass('alertHighlight');
            self.options.customErrorHandler($input);

        },

        _validateThis: function ($inputElement) {

            var $errorContainer = this._findErrorContainer($inputElement),
            errorHighLightClass = this.options.errorLabels.highLight;
            i = 0,
            self = this,
            returnValid = true,
            rulesArray = $inputElement.data('validations').replace(/ /g, "").split(",");

            $errorContainer.find('.alertText').remove();
            $inputElement.closest(errorHighLightClass).removeClass('alertHighlight');

            if (!$inputElement.hasClass('optional') || ($inputElement.hasClass('optional') && $inputElement.val().length > 0)) {

                for (i = 0; i < rulesArray.length; i += 1) {

                    var validationRule = self.options.validationrules[rulesArray[i]];

                    if (validationRule !== undefined) {
                        var isvalid = validationRule.rule.call($inputElement);

                        if (!isvalid) {
                            self._onErrorDoThis($inputElement, rulesArray[i]);
                            returnValid = false;
                            break;
                        }

                    }

                }

                if (returnValid) {
                    self._validationSuccessHandler($inputElement);
                }

            }

            return returnValid;


        },

        formReset: function () {

            var $form = this.element,
              $validationInputs = $form.find('input[data-validations],select[data-validations]'),
              self = this;

            if($form.is("form")){
                $form.get(0).reset();
            }else{
                $form.find('input:text, input:password, input:file, select, textarea').val('');
                $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            }

            $form.find('.alertText').remove();
            $form.find('.alertHighlight').removeClass('alertHighlight');
            //$form.find('.error-border').removeClass('errorBorder');
            $validationInputs.each(function () {

            var $this = $(this),
            inputvalidationRules = $(this).data('validations').replace(/ /g, "").split(','),
            i = 0;

                for (i = 0; i < inputvalidationRules.length; i += 1) {

                    var resetrule = self.options.validationrules[inputvalidationRules[i]]['resetfields'];

                    if (resetrule !== undefined) {
                        resetrule.call($this);
                    }
                }

            });

        },

        formValidate: function () {

            var $form = this.element,
             $validationInputs = $form.find('input[data-validations],select[data-validations]'),
             self = this,
             isValid = true;

            $('.alertText', $form).remove();

            $validationInputs.each(function () {

                var $thisinput = $(this);
                if (!self._validateThis($thisinput)) {
                    isValid = false;
                }

            });

            return isValid;

        },

        _attachValidator: function () {

            var $form = this.element,
            $validationInputs = $form.find('input[data-validations],select[data-validations]'),
            self = this;

            /*********** Validating on Event Bind  ***************/
            $validationInputs.each(function () {

                var $thisinput = $(this),
              eventbind = $thisinput.data('vevent');

                if (eventbind !== undefined) {

                    $thisinput.on(eventbind, function (event) {
                        self._validateThis($thisinput);
                    });


                }

                if ($thisinput.hasClass('nopaste')) {

                    $thisinput.on('paste', function (e) {
                        e.preventDefault();
                        return false;
                    });

                }

            });


            /*********** Validating on Form Submit ***************/

            $form.on('submit form-submit', function (e) {
                var isValid = true;
                $('.alertText', $form).remove();

                $form.find('input[data-validations],select[data-validations]').each(function () {

                    var $thisinput = $(this);
                    if (!self._validateThis($thisinput)) {
                        isValid = false;
                    }

                });


                if (isValid) {
                    return self.options.submitHandler.call(this);
                } else {
                    $("input[type=text]:first", $form).focus();
                    e.preventDefault();
                    return false;
                }
            });

        }

    });

})(jQuery);
/* ************************************************************/