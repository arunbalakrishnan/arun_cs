/*
* Name : Dynadom
* Dependency : Jquery
* Version : 1.0
* Author : Arun Balakrishnan
* Description : A lightweight jquery library , on the lines of Knockout and Angular js but easier to implement
*
*	Easier to learn and work without much learning curve.
*	Uses Models to associate Dom elements using a declarative syntax. Reduces DOM operations
*	Supports Two way data binding 
*	Supports Lazy Binding - Bind data on the DOM only when needed. 
*	Makes the code clean since it reduces the clutter of DOM manipulations in code. 
*	Faster than the conventional DOM manipulations
*
* [Todo] - Remove Jquery dependency
*/

/*
* Usage : Initialize the view model
* var myViewModel = dynaDom( jsModel );
* jsModel is javascript object
*
* Bind to DOM ( examples )
*	<spandata-bind="text:username"></span>
*	<divdata-bind="visible:ifVisible()"><div>
*	<spandata-bind="text:team"data-onbind="team"></span> Two way data binding
*/


var dynaDom = function( viewmodel ) {

	var onValueChangeCallBacks = {},
		onSubscribeCallBacks = {};
	
	var setValue = function( prop,value ) {

		var callbackObject = onValueChangeCallBacks[prop],
			isSet = true,
			oldValue = "";

		if ( callbackObject !== undefined ) {
			if ( callbackObject.fire(value) ) {
				isSet = true;
			}
			else {
				isSet = false;
			}
		}

		if ( isSet === true ) {
			oldValue = viewmodel[prop]
			viewmodel[prop] = value;
			
			// Data OnBind is used to check if other properties are affected by changing this Value.
			var $bindDom = $('[data-onbind*='+ prop +']');

			var onSubscribeArray = onSubscribeCallBacks[prop];
			if ( onSubscribeArray !== undefined && onSubscribeArray.length > 0 ) {
				$.each(onSubscribeArray,function( idx, val ) {
					var functionProperty = onSubscribeArray[idx];
					if ( $.isFunction(functionProperty) ) {
						functionProperty();
					}
				});
			}

			if ( $bindDom.length > 0 ) {
				$bindDom.each( function() {
					bindProperty($(this));
				});
				
			}
		}
	}

	var getValue = function(prop) {
		if ( viewmodel[prop] !== undefined ) {
			return viewmodel[prop];
		}
		return "";
	}

	var subscribe = function( prop, callback ) {
		var onSubscribeArray = onSubscribeCallBacks[prop];
		if ( onSubscribeArray === undefined ) {
			onSubscribeCallBacks[prop] = [];
		}
		onSubscribeCallBacks[prop].push(callback);
	}

	var onValueChanging = function( prop,callback ) {
		var callbackObject = onValueChangeCallBacks[prop];
		if ( callbackObject === undefined ) {
			onValueChangeCallBacks[prop] = $.Callbacks("unique stopOnFalse");
		}

		if ( $.isFunction(callback) ) {
			onValueChangeCallBacks[prop].add(callback);
		}
	}

	var bindProperty = function( $element ) {

		var $this = $element,
			thisDataBindAttr = $this.data('bind'),
			dataBindKeyValue = thisDataBindAttr.split(":"),
			dataBindKey = dataBindKeyValue[0],
			dataBindValue = dataBindKeyValue[1],
			valueToUpdate = viewmodel[dataBindValue] || "";

		var functionArray = /(\w+)\(["]?([A-Za-z0-9 ,']*)?["]?\)/.exec(dataBindValue);
		var functionName,
			functionParams=null;

		if ( functionArray !== null && functionArray.length > 1 ) {
			functionName = functionArray[1];

			if ( functionArray[2] !== undefined ) {
				functionParams = functionArray[2].replace(/'/g,"").split(",");
			}
			valueToUpdate = viewmodel[functionName] || "";
		}



		if ( $.isFunction(valueToUpdate) ) {
			//////////////////////////////////////////////////////////////
			// Checks if the function is in ViewModel and then execute
			//////////////////////////////////////////////////////////////
			valueToUpdate = valueToUpdate.apply(null,functionParams);
		}
		else if( functionName !== undefined ) {
			// Check if global function. Used as a fallback.
			var functionObj = window[functionName];
			if ( $.isFunction(functionObj) ) {
				valueToUpdate = functionObj.apply(null,functionParams);
			}
		}

		
		switch (dataBindKey) {
			case 'text' :
				$this.text(valueToUpdate);
				break;
			case 'html' : 
				$this.html(valueToUpdate);
				break;
			case 'value': 
				$this.val(valueToUpdate);
			break;
			case 'visible' :
				if ( valueToUpdate ) {
					$this.show();
				}
				else {
					$this.hide();
				}
			default: 
				//TODO Arun -> Catch err when the binding did not work.
				break;
		}

	}

	var executeBindings = function( sectionToUpdate ) {
		var $sectionToUpdate = $(sectionToUpdate);
		if ( $sectionToUpdate.length > 0 ) {
			var $bindElements = $sectionToUpdate.find('[data-bind]');
			$bindElements.each(function(){
				bindProperty( $(this) );
			});
		}
	}

	return {
		setValue : setValue,
		getValue : getValue,
		subscribe: subscribe,
		onValueChanging : onValueChanging,
		executeBindings : executeBindings
	}
}